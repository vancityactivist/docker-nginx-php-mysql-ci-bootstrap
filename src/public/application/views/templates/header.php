<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $page_title; ?></title>

<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
